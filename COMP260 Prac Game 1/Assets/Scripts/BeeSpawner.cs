﻿using UnityEngine;
using System.Collections;

public class BeeSpawner : MonoBehaviour {
	public int nBees = 5;
	public float beePeriod;
	private float beePeriodPrivate;
	public bool randomBeeTime;
	public bool chaos;
	public float mibBeePeriod;
	public float maxBeePeriod;
	public BeeMove beePrefab;
	public float xmin, ymin;
	public float width, height;
	public int count =0;

	// Use this for initialization
	void Start () {
		beePeriodPrivate = beePeriod;

//		for (int i = 0; i < nBees; i++) {
//
//			BeeMove bee = Instantiate (beePrefab);
//			bee.transform.parent = transform;
//			bee.gameObject.name = "BEE " + i;
//			float x = xmin + Random.value * width;
//			float y = ymin + Random.value * height;
//
//			bee.transform.position = new Vector2 (x, y);
//		}
	}
	public void DestroyBees(Vector2 centre, float radius) {
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild(i);

			Vector2 v = (Vector2)child.position - centre;
			if (v.magnitude <= radius) {
				//For the particle system. add a varaible that will point to the particle system in the assets folder, thtat is attached to a null. then when destroy is called call spawn the empty at its location, then have it simulate its particle system. Then destroy the empty once the particle system has finished
			
				Destroy (child.gameObject);
			}
		}

	

	}
	// Update is called once per frame
	void Update () {
		if (!randomBeeTime) {
			beePeriodPrivate = beePeriodPrivate - Time.deltaTime;

			if (beePeriodPrivate < 0) {
				BeeMove bee = Instantiate (beePrefab);
				bee.transform.parent = transform;
				bee.gameObject.name = "BEE " + Time.deltaTime;
				float x = xmin + Random.value * width;
				float y = ymin + Random.value * height;
				bee.transform.position = new Vector2 (x, y);
				beePeriodPrivate = beePeriod;
			}
		} else if (randomBeeTime) {
			float beeRandom = mibBeePeriod + Random.value * maxBeePeriod;
			beePeriodPrivate = beeRandom - Time.deltaTime;

			if (beePeriodPrivate < 0) {
				BeeMove bee = Instantiate (beePrefab);
				bee.transform.parent = transform;
				bee.gameObject.name = "BEE " + Time.deltaTime;
				float x = xmin + Random.value * width;
				float y = ymin + Random.value * height;
				bee.transform.position = new Vector2 (x, y);
				beePeriodPrivate = mibBeePeriod + Random.value * maxBeePeriod;

			}
		} else if (chaos) {
			BeeMove bee = Instantiate (beePrefab);
			bee.transform.parent = transform;
			bee.gameObject.name = "BEE " + Time.deltaTime;
			float x = xmin + Random.value * width;
			float y = ymin + Random.value * height;
			bee.transform.position = new Vector2 (x, y);
			beePeriodPrivate = mibBeePeriod + Random.value * maxBeePeriod;
		}
	
	}
}
