﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {
	private BeeSpawner beeSpawner;

	// Use this for initialization
	void Start () {
		beeSpawner = FindObjectOfType<BeeSpawner> ();
	}
	public int Player1 = 0;
	public Vector2 velocity; // in metres per second

	public float maxSpeed = 5.0f;

	public float destroyRadius = 1.0f;

	void Update() {
		if (Input.GetButton ("Fire1")) {
			beeSpawner.DestroyBees (transform.position, destroyRadius);
		}
		// get the input values
		Vector2 direction;
		if (Player1 == 0) {
			direction.x = Input.GetAxis ("Horizontal");
			direction.y = Input.GetAxis ("Vertical");
		} else {
			direction.x = Input.GetAxis ("Horizontal1");
			direction.y = Input.GetAxis ("Vertical1");
		}
		// scale by the maxSpeed parameter
//		Debug.Log ("position = " + transform.position);

		Vector2 velocity = direction * maxSpeed;

		// move the object
		transform.Translate(velocity * Time.deltaTime);
	}

	
}
