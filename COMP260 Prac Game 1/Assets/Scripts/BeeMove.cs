﻿using UnityEngine;
using System.Collections;

public class BeeMove : MonoBehaviour {
	public float minSpeed, maxSpeed;
	public float minTurnSpeed, maxTurnSpeed;
	private float speed;
	private float turnSpeed;

	public Transform target;
	public Vector2 heading = Vector3.right;
	public ParticleSystem explosionPrefab;
	// Use this for initialization
	void Start () {
		PlayerMove player = (PlayerMove) FindObjectOfType (typeof(PlayerMove));
			target = player.transform;
		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate (angle);
		speed = Mathf.Lerp (minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp (minTurnSpeed, maxTurnSpeed, Random.value);
	

	}
	void OnDestroy(){
		ParticleSystem explosion = Instantiate (explosionPrefab);
		explosion.transform.position = transform.position;
		Destroy (explosion.gameObject, explosion.duration);
	}
	void Update(){
		Vector2 direction1 = target.position - transform.position;
		//float distance1 = Vector2.Distance (target.transform.position, transform.position);

		//Debug.Log (distance1);

		float angle = turnSpeed * Time.deltaTime;


		if (direction1.IsOnLeft (heading)) {
				heading = heading.Rotate (angle);
			} else {
				heading = heading.Rotate (-angle);
			}
			transform.Translate (heading * speed * Time.deltaTime);
		} 



}
